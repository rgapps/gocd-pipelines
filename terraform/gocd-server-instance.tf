resource "aws_instance" "gocd-server" {
  count           = var.deploy_dev == true ? 1 : 0
  ami             = var.aws-ami-id
  instance_type   = "t2.medium"
  key_name        = aws_key_pair.aws-key.0.id
  security_groups = [
    aws_security_group.ssh.0.name,
    aws_security_group.internet-access.0.name,
    aws_security_group.gocd-server.0.name
  ]
  root_block_device {
    volume_size = 30
  }

  provisioner "remote-exec" {
    inline = [
      "echo \"Instance is up and running.\""
    ]

    connection {
      host        = self.public_ip
      type        = "ssh"
      user        = "ec2-user"
      private_key = file(var.ssh-key-private)
      timeout     = "2m"
    }
  }

  tags = {
    Name = "gocd-server"
  }
}

resource "null_resource" "gocd-server-provisioner" {
  count    = var.deploy_dev == true ? 1 : 0
  triggers = {
    this_file            = sha1(file("${path.cwd}/gocd-server-instance.tf"))
    playbook             = sha1(file("${path.cwd}/../ansible-playbooks/install-docker.yml"))
    gocd_server_instance = aws_instance.gocd-server.0.id
  }

  depends_on = [
    aws_instance.gocd-server
  ]

  provisioner "local-exec" {
    command = join(" ", [
      "ansible-playbook",
      "--inventory 'ec2-user@${aws_instance.gocd-server.0.public_ip},'",
      "--private-key ${var.ssh-key-private}",
      "../ansible-playbooks/install-docker.yml"
    ])
  }
}

output "gocd-server-ssh" {
  value = var.deploy_dev == true ? "ssh -i '${var.ssh-key-private}' ec2-user@${aws_instance.gocd-server.0.public_ip}" : ""
}
