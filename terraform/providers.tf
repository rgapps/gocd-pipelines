provider "aws" {
  access_key = var.aws-access-key
  secret_key = var.aws-secret-key
  region     = "us-east-1"
}

provider "null" {
}

provider "http" {
}

provider "external" {
}

provider "local" {
}
