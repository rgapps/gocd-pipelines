resource "null_resource" "gocd-server-execution" {
  count    = var.deploy_dev == true ? 1 : 0
  triggers = {
    this_file   = sha1(file("${path.cwd}/gocd-server.tf"))
    playbook    = sha1(file("${path.cwd}/../ansible-playbooks/install-gocd-server.yml"))
    provisioned = null_resource.gocd-server-provisioner.0.id
  }

  depends_on = [
    null_resource.gocd-server-provisioner
  ]

  provisioner "local-exec" {
    command = join(" ", [
      "ansible-playbook",
      "--inventory 'ec2-user@${aws_instance.gocd-server.0.public_ip},'",
      "--private-key ${var.ssh-key-private}",
      "--extra-vars 'SHARED_PATH=${var.remote-shared-path}'",
      "../ansible-playbooks/install-gocd-server.yml"
    ])
  }
}

resource "null_resource" "gocd-server-configuration" {
  count    = var.deploy_dev == true ? 1 : 0
  triggers = {
    playbook  = sha1(file("${path.cwd}/../ansible-playbooks/configure-gocd-server.yml"))
    execution = null_resource.gocd-server-execution.0.id
  }

  depends_on = [
    null_resource.gocd-server-execution
  ]

  provisioner "local-exec" {
    command = join(" ", [
      "ansible-playbook",
      "--inventory 'ec2-user@${aws_instance.gocd-server.0.public_ip},'",
      "--private-key ${var.ssh-key-private}",
      "--extra-vars 'SHARED_PATH=${var.remote-shared-path}'",
      "../ansible-playbooks/configure-gocd-server.yml"
    ])
  }
}

output "gocd-server-host" {
  value = var.deploy_dev == true ? "http://${aws_instance.gocd-server.0.public_ip}:8153" : ""
}
