// this should be equivalent to running a ami in AWS, we can connect to ssh and run ansible notebooks
resource "null_resource" "internal-instance" {
  count    = var.deploy_dev == true ? 1 : 0
  triggers = {
    this_file  = sha1(file("${path.cwd}/internal-instance.tf"))
    volumes    = sha1(join("", [for f in fileset("${path.cwd}/../inputs/ami-equivalent", "*") : filesha1("${path.cwd}/../inputs/ami-equivalent/${f}")]))
    dockerfile = sha1(file("${path.cwd}/../docker-images/ami-equivalent/Dockerfile"))
  }
  provisioner "local-exec" {
    command = "docker rm -f ami-equivalent || true"
  }
  provisioner "local-exec" {
    command = "rm -rf ${var.local-shared-path}/*"
  }
  provisioner "local-exec" {
    command = join(" ", [
      "mkdir ${var.local-shared-path}/ami-equivalent"
    ])
  }
  provisioner "local-exec" {
    command = "cp -rf ../inputs/ami-equivalent/volumes ${var.local-shared-path}/ami-equivalent"
  }
  provisioner "local-exec" {
    command = "sh ../scripts/wait-gitlab-pipelines.sh https://gitlab.com/rgapps/infrastructure dev"
  }
  provisioner "local-exec" {
    command = join(" ", [
      "docker run",
      "--pull always",
      "-d",
      "-p 22:2222",
      "-v /var/run/docker.sock:/var/run/docker.sock",
      "-v ${var.local-shared-path}:${var.local-shared-path}",
      "-v ${var.local-shared-path}/ami-equivalent/volumes:/etc/ssh",
      "-v ${var.local-shared-path}/shared:/etc/shared",
      "--name ami-equivalent",
      "registry.gitlab.com/rgapps/infrastructure/ami-equivalent:dev"
    ])
  }

  provisioner "remote-exec" {
    inline = [
      "echo \"Instance is up and running.\""
    ]

    connection {
      host        = "localhost"
      type        = "ssh"
      user        = "ec2-user"
      private_key = file(var.ssh-key-private)
      timeout     = "2m"
    }
  }
}

data "http" "local-public-ip" {
  url = "https://api.ipify.org/"
}

variable "local-private-ip" {
  type    = string
  default = "172.17.0.1"
}

variable "local-ssh-private-ip" {
  type    = string
  default = "127.0.0.1"
}

variable "local-shared-path" {
  type    = string
  default = "/Users/rgapps"
}

output "internal-instance-ssh" {
  value = var.deploy_dev == true ? "ssh -i ${var.ssh-key-private} ec2-user@${var.local-ssh-private-ip}" : ""
}
