#!/bin/bash
set -eux

cd gocd-pipelines || exit 1

rm gocd-pipelines.gocd.yaml || true
{
  printf "format_version: 10\n"
  cat variables.yaml
  printf "\n"
  cat -- materials.yaml | sed -e '/common:/d'
  printf "\n"
  cat -- templates/* | sed -e '/common:/d'
  printf "\npipelines:\n"
  cat -- pipelines/*/* | sed -e '/format_version: 10/d' | sed -e '/pipelines:/d'
  cat environments.yaml
} >>gocd-pipelines.gocd.yaml

cd ..
