#!/bin/bash
set -eux

repository=git@gitlab.com:rgapps/infrastructure.git

if [ -d "repo" ]; then
  rm -rf repo
fi

git clone --depth 1 --branch dev "${repository}" repo
cd repo || exit 1

gitCryptFile="/shared/gocd/godata/config/git-crypt-key"
backupPath="/shared/gocd/godata/artifacts/serverBackups/"

if [ ! -f "${gitCryptFile}" ]; then
  echo "ERROR: ${gitCryptFile} file not  found"
  exit 1
fi
if [ ! -d "${backupPath}" ]; then
  echo "ERROR: ${backupPath} file not  found"
  exit 1
fi

git-crypt unlock /shared/gocd/godata/config/git-crypt-key

# shellcheck disable=SC2012
mostRecent=$(ls -Art ${backupPath} | tail -n 1)

rm -rf inputs/backups/gocd/*
ls -ltr ${backupPath}
ls -ltr ${backupPath}"${mostRecent}"
cp ${backupPath}"${mostRecent}"/* inputs/backups/gocd
echo "$(date)" > inputs/backups/gocd/time

# commit state change!
if [ "$(git log -1 --pretty=%B | sed -n 1p)" == "CICD: Update Gocd Backup" ]; then
  git reset --soft HEAD~1
fi

git add inputs/backups/gocd/*
echo "CICD: Update Gocd Backup" | git commit -F -
git push -f
cd ..
