#!/bin/bash
set -eux

if [ -z ${1+x} ] || [ -z ${2+x} ]; then
  echo "ERROR: two parameters required: repository and branch"
  exit 1
fi

repository=$1
branch=$2

if [ "${branch}" = "latest" ]; then
  branch=$(curl -s "${repository}/raw/master/version.txt")
fi

n=0
until curl -sL "${repository}"/badges/"${branch}"/pipeline.svg | grep passed; do
  echo "Waiting for pipeline (${repository}/badges/${branch}/pipeline.svg)......"
  n=$((n+1))
  if [ "$n" = "60" ]; then
    exit 1
  fi
  sleep 5s
done
