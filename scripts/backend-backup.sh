#!/bin/bash
set -eux

if [ $# -eq 0 ]; then
  echo "ERROR: need a parameter with the prod ip"
  exit 1
fi

prodIp=$1

# connect to prod and get backup
ssh -L 27018:localhost:27017 -i '~/.ssh/aws_key.pem' ec2-user@${prodIp} -f -N
rm -rf inputs/backups/database/*
mongodump --port 27018 -o inputs/backups/database -d general-backend
echo "$(date)" > inputs/backups/database/time

kill $(ps aux | grep ec2-user@${prodIp} | grep -v grep | awk '{print $2}')

# commit state change!
if [ "$(git log -1 --pretty=%B | sed -n 1p)" == "Update Prod Backup!" ]; then
  git reset --soft HEAD~1
fi

git add inputs/backups/database/*

echo "Update Prod Backup!" | git commit -F -
cd ..
