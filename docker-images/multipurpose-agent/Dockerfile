FROM gocd/gocd-agent-ubuntu-20.04:v22.3.0

USER root

ENV NODE_VERSION 20.x
ENV JAVA_VERSION openjdk-13-jre-headless
ENV DOCKER_VERSION 19.03.5
ENV DOCKER_COMPOSE_VERSION 1.28.2

SHELL ["/bin/bash", "-euxo", "pipefail", "-c"]
# hadolint ignore=DL3008
RUN apt-get update --no-install-recommends && apt-get install -y --no-install-recommends curl bc gnupg git-crypt \
# java
    default-jre default-jdk ${JAVA_VERSION} && \
# docker: https://stackoverflow.com/questions/44803773/install-docker-using-a-dockerfile
    curl -sL "https://download.docker.com/linux/static/stable/x86_64/docker-${DOCKER_VERSION}.tgz" \
    | tar -xzC /usr/local/bin --strip=1 docker/docker && \
# docker-compose: https://docs.docker.com/compose/install/
    curl -sL "https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m)" \
    -o /usr/local/bin/docker-compose && chmod +x /usr/local/bin/docker-compose && \
# nodejs : https://github.com/nodesource/distributions
    curl -sL https://deb.nodesource.com/setup_${NODE_VERSION} | bash - && \
    apt-get install -y --no-install-recommends nodejs gcc g++ make && \
# gitlab git pull/push/merge
    mkdir /home/go/.ssh && \
    ssh-keyscan gitlab.com > /home/go/.ssh/known_hosts && \
# clean
    rm -rf /var/lib/apt/lists/* && \
    apt-get -y autoremove && apt-get -y clean && \
    chmod 777 /home/go/.ssh && \
    chmod 777 /home/go/.ssh/known_hosts


USER go
